package repository

import (
    "sync"
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/lib"
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/lib/utils"
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/models"
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/domains"
)

type GroupRepository struct {
    mutex       *sync.RWMutex
    logger      lib.Logger
    groups      map[uint] *utils.LinkedListNode[*models.Group]
    groupsList  *utils.LinkedList[*models.Group]
}

func NewGroupRepository(logger lib.Logger) GroupRepository {
    return GroupRepository{
        mutex: &sync.RWMutex{},
        logger:   logger,
        groups: make(map[uint] *utils.LinkedListNode[*models.Group]),
        groupsList: utils.NewLinkedList[*models.Group](),
    }
}

func (this *GroupRepository) Find(id uint) (*models.Group, error) {
    this.mutex.RLock()
    defer this.mutex.RUnlock()

    val, _ := this.groups[id]
    if val != nil {
        return val.GetValue(), nil
    }

    return nil, domains.ErrNotFound
}


func (this *GroupRepository) FindAll() []*models.Group {
    this.mutex.RLock()
    defer this.mutex.RUnlock()

    result := make([]*models.Group, len(this.groups))

    for _, group := range this.groups {
        result = append(result, group.GetValue())
    }

    return result
}

func (this *GroupRepository) Create(group *models.Group) error {
    groupRepositoryEntry := utils.NewLinkedListNode[*models.Group](group)

    this.mutex.Lock()
    defer this.mutex.Unlock()

    existingGroup, _ := this.groups[group.ID]
    if existingGroup != nil {
        return domains.ErrDuplicatedID
    }

    this.groups[group.ID] = groupRepositoryEntry
    return nil
}

func (this *GroupRepository) Save(group *models.Group) error {
    this.mutex.Lock()
    defer this.mutex.Unlock()

    groupRepositoryEntry, _ := this.groups[group.ID]
    if groupRepositoryEntry == nil {
        this.logger.Error("Updating group which does not exist")
        return domains.ErrNotFound
    }

    if (group.AssignedTo != nil) {
        existingGroupPendingList := groupRepositoryEntry.GetParentList()
        if existingGroupPendingList != nil {
            existingGroupPendingList.Remove(groupRepositoryEntry)
        }
    }

    groupRepositoryEntry.SetValue(group)
    this.groups[group.ID] = groupRepositoryEntry
    return nil
}

func (this *GroupRepository) Delete(id uint) error {
    this.mutex.Lock()
    defer this.mutex.Unlock()

    groupRepositoryEntry, _ := this.groups[id]
    if groupRepositoryEntry != nil {
        parentList := groupRepositoryEntry.GetParentList()
        if parentList != nil {
            parentList.Remove(groupRepositoryEntry)
        }
    }

    delete(this.groups, id)
    return nil
}

func (this *GroupRepository) DeleteAll() error {
    this.mutex.Lock()
    defer this.mutex.Unlock()

    this.groupsList.Clear()
    utils.Clear(this.groups)

    return nil
}

func (this *GroupRepository) GetPendingList() *utils.LinkedList[*models.Group] {
    return this.groupsList
}

func (this *GroupRepository) EnqueueIntoPendingList(group *models.Group) error {
    this.mutex.Lock()
    defer this.mutex.Unlock()

    val, _ := this.groups[group.ID]

    if val == nil {
        return domains.ErrNotFound
    }

    this.groupsList.AddLast(val)
    return nil
}

func (this *GroupRepository) GetNextGroupByMaxPeople(passengerCount uint) (*models.Group, error) {
    this.mutex.Lock()
    defer this.mutex.Unlock()

    var nextGroup *utils.LinkedListNode[*models.Group]
    var currentGroup *utils.LinkedListNode[*models.Group] = this.groupsList.First()

    var result *models.Group

    for currentGroup != nil {
        nextGroup = currentGroup.GetNextNode()

        currentGroupValue := currentGroup.GetValue()

        if currentGroupValue.People <= passengerCount {
            result = currentGroupValue
            this.groupsList.Remove(currentGroup)
            break
        }

        currentGroup = nextGroup
    }

    return result, nil
}
