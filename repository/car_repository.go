package repository

import (
    "sync"
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/lib"
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/lib/utils"
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/models"
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/domains"
)

type CarRepository struct {
    mutex *sync.RWMutex
    logger lib.Logger
    cars map[uint] *utils.LinkedListNode[*models.Car]
    carsByAvailableSeats map[uint] *utils.LinkedList[*models.Car]
}

func NewCarRepository(logger lib.Logger) CarRepository {
    carsByAvailableSeats := make(map[uint] *utils.LinkedList[*models.Car])

    for i := uint(1); i <= domains.MaxSeats; i++ {
        carsByAvailableSeats[i] = utils.NewLinkedList[*models.Car]()
    }

    return CarRepository{
        mutex: &sync.RWMutex{},
        logger:   logger,
        cars: make(map[uint] *utils.LinkedListNode[*models.Car]),
        carsByAvailableSeats: carsByAvailableSeats,
    }
}

func (this *CarRepository) Find(id uint) (*models.Car, error) {
    this.mutex.RLock()
    defer this.mutex.RUnlock()

    val, _ := this.cars[id]
    if val != nil {
        return val.GetValue(), nil
    }

    return nil, domains.ErrNotFound
}

func (this *CarRepository) FindAll() []*models.Car {
    this.mutex.RLock()
    defer this.mutex.RUnlock()

    result := make([]*models.Car, len(this.cars))

    for _, car := range this.cars {
        result = append(result, car.GetValue())
    }

    return result
}

func (this *CarRepository) Create(car *models.Car) error {
    carRepositoryEntry := utils.NewLinkedListNode[*models.Car](car)

    this.mutex.Lock()
    defer this.mutex.Unlock()

    existingCar, _ := this.cars[car.ID]
    if existingCar != nil {
        return domains.ErrDuplicatedID
    }

    car.AvailableSeats = car.MaxSeats

    this.cars[car.ID] = carRepositoryEntry

    if car.AvailableSeats > 0 && car.AvailableSeats <= domains.MaxSeats {
        this.carsByAvailableSeats[car.AvailableSeats].AddLast(carRepositoryEntry)
    }

    return nil
}

func (this *CarRepository) FindNextWithAvailableSeats(availableSeats uint) *models.Car {
    this.mutex.Lock()
    defer this.mutex.Unlock()

    carNode := this.carsByAvailableSeats[availableSeats].Shift()
    if carNode == nil {
        return nil
    }

    return carNode.GetValue()
}

func (this *CarRepository) Save(car *models.Car) error {
    this.mutex.Lock()
    defer this.mutex.Unlock()

    carRepositoryEntry, _ := this.cars[car.ID]
    if carRepositoryEntry == nil {
        this.logger.Error("Updating car which does not exist")
        return domains.ErrNotFound
    }

    if car.AvailableSeats > 0 {
        if car.AvailableSeats > domains.MaxSeats {
            this.logger.Fatal("Available seats greater than maxseats, this should never happen")
        }
    }

    carRepositoryEntry.SetValue(car)
    this.cars[car.ID] = carRepositoryEntry

    return nil
}

func (this *CarRepository) Delete(id uint) error {
    this.mutex.Lock()
    defer this.mutex.Unlock()

    carRepositoryEntry, _ := this.cars[id]

    if carRepositoryEntry != nil {
        parentList := carRepositoryEntry.GetParentList()
        if parentList != nil {
            parentList.Remove(carRepositoryEntry)
        }
    }

    delete(this.cars, id)

    return nil
}

func (this *CarRepository) DeleteAll() error {
    this.mutex.Lock()
    defer this.mutex.Unlock()

    for i := uint(1); i <= domains.MaxSeats; i++ {
        this.carsByAvailableSeats[i].Clear()
    }

    utils.Clear(this.cars)

    return nil
}

func (this *CarRepository) EnqueueIntoAvailableList(car *models.Car) error {

    this.mutex.Lock()
    defer this.mutex.Unlock()

    carRepositoryEntry, _ := this.cars[car.ID]

    if carRepositoryEntry == nil {
        return domains.ErrNotFound
    }

    parentList := carRepositoryEntry.GetParentList()

    var destinationList *utils.LinkedList[*models.Car]
    if car.AvailableSeats > 0 {
        destinationList = this.carsByAvailableSeats[car.AvailableSeats]
    }

    if parentList != destinationList {
        if parentList != nil {
            parentList.Remove(carRepositoryEntry)
        }
        if destinationList != nil {
            destinationList.AddLast(carRepositoryEntry)
        }
    }

    return nil
}

func (this *CarRepository) UnqueueFromAvailableList(car *models.Car) error {

    this.mutex.Lock()
    defer this.mutex.Unlock()

    carRepositoryEntry, _ := this.cars[car.ID]

    if carRepositoryEntry == nil {
        return domains.ErrNotFound
    }

    parentList := carRepositoryEntry.GetParentList()

    if parentList != nil {
        parentList.Remove(carRepositoryEntry)
    }

    return nil
}
