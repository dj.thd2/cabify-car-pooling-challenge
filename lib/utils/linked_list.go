package utils

import (
    "errors"
    "sync"
)

// Implements a concurrent safe linked list
type LinkedList[T any] struct {
    mutex *sync.RWMutex
    first *LinkedListNode[T]
    last *LinkedListNode[T]
    count int
}

var LinkedListErrInvalidNode = errors.New("node belongs to other list")

// Initialize linked list
func NewLinkedList[T any]() *LinkedList[T] {
    return &LinkedList[T] {mutex: &sync.RWMutex{}, first: nil, last: nil, count: 0}
}

// Add an element to the end of the list
func (this *LinkedList[T]) AddLast(node *LinkedListNode[T]) error {
    this.mutex.Lock()
    defer this.mutex.Unlock()

    node.Lock()
    defer node.Unlock()

    if node.parentList != nil {
        return LinkedListErrInvalidNode
    }

    var prevLast *LinkedListNode[T]
    if this.last != nil {
        prevLast = this.last.Lock()
        defer prevLast.Unlock()
    }

    this.last = node
    node.SetParentListLocked(this, prevLast, nil)
    this.count++

    if prevLast == nil {
        this.first = node
    } else {
        prevLast.next = node
    }

    return nil
}

// Add an element to the beginning of the list
func (this *LinkedList[T]) AddFirst(node *LinkedListNode[T]) error {
    this.mutex.Lock()
    defer this.mutex.Unlock()

    node.Lock()
    defer node.Unlock()

    if node.parentList != nil {
        return LinkedListErrInvalidNode
    }

    var prevFirst *LinkedListNode[T]
    if this.first != nil {
        prevFirst = this.first.Lock()
        defer prevFirst.Unlock()
    }

    this.first = node
    node.SetParentListLocked(this, nil, prevFirst)
    this.count++

    if prevFirst == nil {
        this.last = node
    } else {
        prevFirst.prev = node
    }

    return nil
}

// Remove an element anywhere in the list without locking the list or the removed item
func (this *LinkedList[T]) RemoveLocked(node *LinkedListNode[T]) error {
    if node.parentList != this {
        return LinkedListErrInvalidNode
    }

    var prev *LinkedListNode[T]
    if node.prev != nil {
        prev = node.prev.Lock()
        defer prev.Unlock()
    }

    var next *LinkedListNode[T]
    if node.next != nil {
        next = node.next.Lock()
        defer next.Unlock()
    }

    this.count--
    node.SetParentListLocked(nil, nil, nil)

    if prev == nil {
        this.first = next
    } else {
        prev.next = next
    }

    if next == nil {
        this.last = prev
    } else {
        next.prev = prev
    }

    return nil
}

// Remove an element anywhere in the list
func (this *LinkedList[T]) Remove(node *LinkedListNode[T]) error {
    this.mutex.Lock()
    defer this.mutex.Unlock()

    node.Lock()
    defer node.Unlock()

    return this.RemoveLocked(node)
}

// Get an element from the beginning of the list and remove it from the list
func (this *LinkedList[T]) Shift() *LinkedListNode[T] {
    this.mutex.Lock()
    defer this.mutex.Unlock()

    var result *LinkedListNode[T]

    if this.first != nil {
        result = this.first.Lock()
        defer result.Unlock()
        this.RemoveLocked(result)
    }

    return result
}

// Get an element from the end of the list and remove it from the list
func (this *LinkedList[T]) Pop() *LinkedListNode[T] {
    this.mutex.Lock()
    defer this.mutex.Unlock()

    var result *LinkedListNode[T]
    if this.last != nil {
        result = this.last.Lock()
        defer result.Unlock()
        this.RemoveLocked(result)
    }

    return result
}

// Remove all the items from the list
func (this *LinkedList[T]) Clear() {
    this.mutex.Lock()
    defer this.mutex.Unlock()

    var currentNode *LinkedListNode[T]
    if this.first != nil {
        currentNode = this.first.Lock()
    }
    for currentNode != nil {
        var nextNode *LinkedListNode[T]
        if currentNode.next != nil {
            nextNode = currentNode.next.Lock()
        }
        currentNode.SetParentListLocked(nil, nil, nil)
        currentNode.Unlock()
        currentNode = nextNode
    }
    this.first = nil
    this.last = nil
    this.count = 0

}

// Get first item of the list
func (this *LinkedList[T]) First() *LinkedListNode[T] {
    this.mutex.RLock()
    defer this.mutex.RUnlock()
    return this.first
}

// Get last item of the list
func (this *LinkedList[T]) Last() *LinkedListNode[T] {
    this.mutex.RLock()
    defer this.mutex.RUnlock()
    return this.last
}

// Get list size counter
func (this *LinkedList[T]) Count() int {
    this.mutex.RLock()
    defer this.mutex.RUnlock()
    return this.count
}
