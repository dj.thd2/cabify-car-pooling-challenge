package utils

import (
    "sync"
)

// Implements a concurrent safe linked list node
type LinkedListNode[T any] struct {
    value T
    mutex *sync.RWMutex
    parentList *LinkedList[T]
    prev *LinkedListNode[T]
    next *LinkedListNode[T]
}

// Initialize linked list node
func NewLinkedListNode[T any](value T) *LinkedListNode[T] {
    return &LinkedListNode[T] {value: value, mutex: &sync.RWMutex{}, parentList: nil, prev: nil, next: nil}
}

// Get the mutex lock on the node and return the node itself
func (this *LinkedListNode[T]) Lock() *LinkedListNode[T] {
    this.mutex.Lock()
    return this
}

// Unlock the mutex of the node
func (this *LinkedListNode[T]) Unlock() {
    this.mutex.Unlock()
}

// Get the read mutex lock on the node and return the node itself
func (this *LinkedListNode[T]) RLock() *LinkedListNode[T] {
    this.mutex.RLock()
    return this
}

// Unlock the read mutex of the node
func (this *LinkedListNode[T]) RUnlock() {
    this.mutex.RUnlock()
}

// Get parent list pointer (to be used when the node is already locked)
func (this *LinkedListNode[T]) GetParentListLocked() *LinkedList[T] {
    return this.parentList
}

// Get parent list pointer
func (this *LinkedListNode[T]) GetParentList() *LinkedList[T] {
    this.mutex.RLock()
    defer this.mutex.RUnlock()
    return this.parentList
}

// Get next node
func (this *LinkedListNode[T]) GetNextNode() *LinkedListNode[T] {
    this.mutex.RLock()
    defer this.mutex.RUnlock()
    return this.next
}

// Get prev node
func (this *LinkedListNode[T]) GetPrevNode() *LinkedListNode[T] {
    this.mutex.RLock()
    defer this.mutex.RUnlock()
    return this.prev
}

// Get node value (to be used when the node is already locked)
func (this *LinkedListNode[T]) GetValueLocked() T {
    return this.value
}

// Get the node value
func (this *LinkedListNode[T]) GetValue() T {
    this.mutex.RLock()
    defer this.mutex.RUnlock()
    return this.value
}

// Set node value (to be used when the node is already locked)
func (this *LinkedListNode[T]) SetValueLocked(value T) {
    this.value = value
}

// Set node value
func (this *LinkedListNode[T]) SetValue(value T) {
    this.mutex.Lock()
    defer this.mutex.Unlock()
    this.value = value
}

// Set the node parent list and previous/next nodes (to be used when the node is already locked)
func (this *LinkedListNode[T]) SetParentListLocked(parentList *LinkedList[T], prevNode *LinkedListNode[T], nextNode *LinkedListNode[T]) {
    this.parentList = parentList
    this.prev = prevNode
    this.next = nextNode
}

// Set the node parent list and previous/next nodes
func (this *LinkedListNode[T]) SetParentList(parentList *LinkedList[T], prevNode *LinkedListNode[T], nextNode *LinkedListNode[T]) {
    this.mutex.Lock()
    defer this.mutex.Unlock()
    this.SetParentListLocked(parentList, prevNode, nextNode)
}
