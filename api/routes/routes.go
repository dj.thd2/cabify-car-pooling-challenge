package routes

import "go.uber.org/fx"

var Module = fx.Options(
    fx.Provide(NewCarPoolRoutes),
    fx.Provide(NewSwaggerRoutes),
    fx.Provide(NewRoutes),
)

type Routes []Route

type Route interface {
    Setup()
}

func NewRoutes(
    carPoolRoutes CarPoolRoutes,
    swaggerRoutes SwaggerRoutes,
) Routes {
    return Routes{
        carPoolRoutes,
        swaggerRoutes,
    }
}

func (r Routes) Setup() {
    for _, route := range r {
        route.Setup()
    }
}
