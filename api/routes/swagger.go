package routes

import (
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/api/controllers"
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/lib"
)

type SwaggerRoutes struct {
    logger         lib.Logger
    handler        lib.RequestHandler
    swaggerController controllers.SwaggerController
}

func (this SwaggerRoutes) Setup() {
    this.logger.Info("Setting up swagger routes")
    this.swaggerController.Setup(this.handler.Gin)
}

func NewSwaggerRoutes(
    logger lib.Logger,
    handler lib.RequestHandler,
    swaggerController controllers.SwaggerController,
) SwaggerRoutes {
    return SwaggerRoutes{
        handler:        handler,
        logger:         logger,
        swaggerController: swaggerController,
    }
}
