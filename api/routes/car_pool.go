package routes

import (
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/api/controllers"
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/lib"
)

type CarPoolRoutes struct {
    logger         lib.Logger
    handler        lib.RequestHandler
    carPoolController controllers.CarPoolController
}

func (this CarPoolRoutes) Setup() {
    this.logger.Info("Setting up car pool routes")
    this.carPoolController.Setup(this.handler.Gin)
}

func NewCarPoolRoutes(
    logger lib.Logger,
    handler lib.RequestHandler,
    carPoolController controllers.CarPoolController,
) CarPoolRoutes {
    return CarPoolRoutes{
        handler:        handler,
        logger:         logger,
        carPoolController: carPoolController,
    }
}
