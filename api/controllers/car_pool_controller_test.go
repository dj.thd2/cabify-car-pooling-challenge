package controllers

import (
    "net/http"
    "net/http/httptest"
    "strings"
    "testing"

    "github.com/stretchr/testify/assert"
    "github.com/stretchr/testify/require"

    "go.uber.org/fx"
    "go.uber.org/fx/fxtest"

    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/lib"
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/repository"
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/services"
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/api/middlewares"
)

var TestCommonModules = fx.Options(
    Module,
    lib.Module,
    services.Module,
    repository.Module,
    middlewares.Module,
)

type TestCaseRunner interface{}

func setupTestCase(handler lib.RequestHandler, logger lib.Logger, carPoolController CarPoolController) {
    logger.Info("Setup test case")
    carPoolController.Setup(handler.Gin)
}

func runTestCase(t *testing.T, runner TestCaseRunner) {
    app := fxtest.New(t, TestCommonModules, fx.Invoke(setupTestCase), fx.Invoke(runner))

    defer app.RequireStart().RequireStop()
    require.NoError(t, app.Err())
}

func executeRequest(handler lib.RequestHandler, req *http.Request) *httptest.ResponseRecorder {
    result := httptest.NewRecorder()
    handler.Gin.ServeHTTP(result, req)
    return result
}

func TestStatus(t *testing.T) {
    runTestCase(t, func(
        handler lib.RequestHandler,
    ) {
        req, _ := http.NewRequest("GET", "/status", nil)

        w := executeRequest(handler, req)
        assert.Equal(t, 200, w.Code)
        assert.Equal(t, `{"status":"ok"}`, w.Body.String())
    })
}


func TestAPI(t *testing.T) {
    runTestCase(t, func(
        handler lib.RequestHandler,
    ) {
        // Test resetCars
        req, _ := http.NewRequest("PUT", "/cars", strings.NewReader(`
        [
            { "id": 1, "seats": 4 },
            { "id": 2, "seats": 6 }
        ]`))
        req.Header = map[string][]string{"Content-Type": {"application/json"}}

        w := executeRequest(handler, req)
        assert.Equal(t, 200, w.Code)

        // Test create journey
        req, _ = http.NewRequest("POST", "/journey", strings.NewReader(`
        { "id": 1, "people": 4 }
        `))
        req.Header = map[string][]string{"Content-Type": {"application/json"}}

        w = executeRequest(handler, req)
        assert.Equal(t, 202, w.Code)

        // Test locate
        req, _ = http.NewRequest("POST", "/locate", strings.NewReader("ID=1"))
        req.Header = map[string][]string{"Content-Type": {"application/x-www-form-urlencoded"}}

        w = executeRequest(handler, req)
        assert.Equal(t, 200, w.Code)
        assert.Equal(t, `{"id":1,"seats":4,"availableSeats":0}`, string(w.Body.Bytes()))

        // Test dropoff
        req, _ = http.NewRequest("POST", "/dropoff", strings.NewReader("ID=1"))
        req.Header = map[string][]string{"Content-Type": {"application/x-www-form-urlencoded"}}

        w = executeRequest(handler, req)
        assert.Equal(t, 200, w.Code)
    })
}

func TestResetCarsInvalidMediaType(t *testing.T) {
    runTestCase(t, func(
        handler lib.RequestHandler,
    ) {
        // Test resetCars
        req, _ := http.NewRequest("PUT", "/cars", strings.NewReader(`
        [
            { "id": 1, "seats": 4 },
            { "id": 2, "seats": 6 }
        ]`))
        req.Header = map[string][]string{"Content-Type": {"text/xml"}}

        w := executeRequest(handler, req)
        assert.Equal(t, 415, w.Code)
    })
}

func TestResetCarsInvalidJson(t *testing.T) {
    runTestCase(t, func(
        handler lib.RequestHandler,
    ) {
        // Test resetCars
        req, _ := http.NewRequest("PUT", "/cars", strings.NewReader(`
        [
            { "id": 1, "seats": 4 },
            { "id": 2, "seats": invalid
        ]`))
        req.Header = map[string][]string{"Content-Type": {"application/json"}}

        w := executeRequest(handler, req)
        assert.Equal(t, 400, w.Code)
    })
}

func TestResetCarsDuplicatedId(t *testing.T) {
    runTestCase(t, func(
        handler lib.RequestHandler,
    ) {
        // Test resetCars
        req, _ := http.NewRequest("PUT", "/cars", strings.NewReader(`
        [
            { "id": 1, "seats": 4 },
            { "id": 1, "seats": 6 }
        ]`))
        req.Header = map[string][]string{"Content-Type": {"application/json"}}

        w := executeRequest(handler, req)
        assert.Equal(t, 400, w.Code)
    })
}

func TestNewJourneyInvalidMediaType(t *testing.T) {
    runTestCase(t, func(
        handler lib.RequestHandler,
    ) {
        req, _ := http.NewRequest("POST", "/journey", strings.NewReader(`
        { "id": 1, "people": 4 }
        `))
        req.Header = map[string][]string{"Content-Type": {"text/xml"}}

        w := executeRequest(handler, req)
        assert.Equal(t, 415, w.Code)
    })
}

func TestNewJourneyInvalidJson(t *testing.T) {
    runTestCase(t, func(
        handler lib.RequestHandler,
    ) {
        req, _ := http.NewRequest("POST", "/journey", strings.NewReader(`
        { "id": 1, "people": invalid
        `))
        req.Header = map[string][]string{"Content-Type": {"application/json"}}

        w := executeRequest(handler, req)
        assert.Equal(t, 400, w.Code)
    })
}

func TestNewJourneyDuplicatedId(t *testing.T) {
    runTestCase(t, func(
        handler lib.RequestHandler,
    ) {
        req, _ := http.NewRequest("POST", "/journey", strings.NewReader(`
        { "id": 1, "people": 4 }
        `))
        req.Header = map[string][]string{"Content-Type": {"application/json"}}

        w := executeRequest(handler, req)
        assert.Equal(t, 202, w.Code)

        req, _ = http.NewRequest("POST", "/journey", strings.NewReader(`
        { "id": 1, "people": 4 }
        `))
        req.Header = map[string][]string{"Content-Type": {"application/json"}}

        w = executeRequest(handler, req)
        assert.Equal(t, 400, w.Code)
    })
}

func TestDropoffInvalidMediaType(t *testing.T) {
    runTestCase(t, func(
        handler lib.RequestHandler,
    ) {
        req, _ := http.NewRequest("POST", "/dropoff", strings.NewReader("ID=1"))
        req.Header = map[string][]string{"Content-Type": {"text/xml"}}

        w := executeRequest(handler, req)
        assert.Equal(t, 415, w.Code)
    })
}

func TestDropoffInvalidFormData(t *testing.T) {
    runTestCase(t, func(
        handler lib.RequestHandler,
    ) {
        req, _ := http.NewRequest("POST", "/dropoff", strings.NewReader("invalid"))
        req.Header = map[string][]string{"Content-Type": {"application/x-www-form-urlencoded"}}

        w := executeRequest(handler, req)
        assert.Equal(t, 400, w.Code)
    })
}

func TestDropoffNotFound(t *testing.T) {
    runTestCase(t, func(
        handler lib.RequestHandler,
    ) {
        req, _ := http.NewRequest("POST", "/dropoff", strings.NewReader("ID=100"))
        req.Header = map[string][]string{"Content-Type": {"application/x-www-form-urlencoded"}}

        w := executeRequest(handler, req)
        assert.Equal(t, 404, w.Code)
    })
}

func TestDropoffJourneyWithoutCar(t *testing.T) {
    runTestCase(t, func(
        handler lib.RequestHandler,
    ) {
        req, _ := http.NewRequest("POST", "/journey", strings.NewReader(`
        { "id": 1, "people": 4 }
        `))
        req.Header = map[string][]string{"Content-Type": {"application/json"}}

        w := executeRequest(handler, req)
        assert.Equal(t, 202, w.Code)

        req, _ = http.NewRequest("POST", "/dropoff", strings.NewReader("ID=1"))
        req.Header = map[string][]string{"Content-Type": {"application/x-www-form-urlencoded"}}

        w = executeRequest(handler, req)
        assert.Equal(t, 204, w.Code)
    })
}

func TestDropoffJourneyWithCar(t *testing.T) {
    runTestCase(t, func(
        handler lib.RequestHandler,
    ) {
        req, _ := http.NewRequest("PUT", "/cars", strings.NewReader(`
        [
            { "id": 1, "seats": 4 }
        ]`))
        req.Header = map[string][]string{"Content-Type": {"application/json"}}

        w := executeRequest(handler, req)
        assert.Equal(t, 200, w.Code)

        req, _ = http.NewRequest("POST", "/journey", strings.NewReader(`
        { "id": 1, "people": 4 }
        `))
        req.Header = map[string][]string{"Content-Type": {"application/json"}}

        w = executeRequest(handler, req)
        assert.Equal(t, 202, w.Code)

        req, _ = http.NewRequest("POST", "/dropoff", strings.NewReader("ID=1"))
        req.Header = map[string][]string{"Content-Type": {"application/x-www-form-urlencoded"}}

        w = executeRequest(handler, req)
        assert.Equal(t, 200, w.Code)
    })
}

func TestLocateInvalidMediaType(t *testing.T) {
    runTestCase(t, func(
        handler lib.RequestHandler,
    ) {
        req, _ := http.NewRequest("POST", "/locate", strings.NewReader("ID=1"))
        req.Header = map[string][]string{"Content-Type": {"text/xml"}}

        w := executeRequest(handler, req)
        assert.Equal(t, 415, w.Code)
    })
}

func TestLocateInvalidFormData(t *testing.T) {
    runTestCase(t, func(
        handler lib.RequestHandler,
    ) {
        req, _ := http.NewRequest("POST", "/locate", strings.NewReader("invalid"))
        req.Header = map[string][]string{"Content-Type": {"application/x-www-form-urlencoded"}}

        w := executeRequest(handler, req)
        assert.Equal(t, 400, w.Code)
    })
}

func TestLocateNotFound(t *testing.T) {
    runTestCase(t, func(
        handler lib.RequestHandler,
    ) {
        req, _ := http.NewRequest("POST", "/locate", strings.NewReader("ID=100"))
        req.Header = map[string][]string{"Content-Type": {"application/x-www-form-urlencoded"}}

        w := executeRequest(handler, req)
        assert.Equal(t, 404, w.Code)
    })
}

func TestLocateJourneyWithoutCar(t *testing.T) {
    runTestCase(t, func(
        handler lib.RequestHandler,
    ) {
        req, _ := http.NewRequest("POST", "/journey", strings.NewReader(`
        { "id": 1, "people": 4 }
        `))
        req.Header = map[string][]string{"Content-Type": {"application/json"}}

        w := executeRequest(handler, req)
        assert.Equal(t, 202, w.Code)

        req, _ = http.NewRequest("POST", "/locate", strings.NewReader("ID=1"))
        req.Header = map[string][]string{"Content-Type": {"application/x-www-form-urlencoded"}}

        w = executeRequest(handler, req)
        assert.Equal(t, 204, w.Code)
    })
}

func TestLocateJourneyWithCar(t *testing.T) {
    runTestCase(t, func(
        handler lib.RequestHandler,
    ) {
        req, _ := http.NewRequest("PUT", "/cars", strings.NewReader(`
        [
            { "id": 1, "seats": 4 }
        ]`))
        req.Header = map[string][]string{"Content-Type": {"application/json"}}

        w := executeRequest(handler, req)
        assert.Equal(t, 200, w.Code)

        req, _ = http.NewRequest("POST", "/journey", strings.NewReader(`
        { "id": 1, "people": 4 }
        `))
        req.Header = map[string][]string{"Content-Type": {"application/json"}}

        w = executeRequest(handler, req)
        assert.Equal(t, 202, w.Code)

        req, _ = http.NewRequest("POST", "/locate", strings.NewReader("ID=1"))
        req.Header = map[string][]string{"Content-Type": {"application/x-www-form-urlencoded"}}

        w = executeRequest(handler, req)
        assert.Equal(t, 200, w.Code)
        assert.Equal(t, `{"id":1,"seats":4,"availableSeats":0}`, string(w.Body.Bytes()))
    })
}

func TestInvalidRequest(t *testing.T) {
    runTestCase(t, func(
        handler lib.RequestHandler,
    ) {
        req, _ := http.NewRequest("PUT", "/invalid", strings.NewReader(`
        [
            { "id": 1, "seats": 4 }
        ]`))
        req.Header = map[string][]string{"Content-Type": {"application/json"}}

        w := executeRequest(handler, req)
        assert.Equal(t, 400, w.Code)
    })
}
