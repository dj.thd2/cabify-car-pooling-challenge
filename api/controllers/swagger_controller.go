package controllers

import (
    docs "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/docs"
    swaggerfiles "github.com/swaggo/files"
    ginSwagger "github.com/swaggo/gin-swagger"

    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/lib"

    "github.com/gin-gonic/gin"
)

type SwaggerController struct {
    logger                       lib.Logger
}

func NewSwaggerController(logger lib.Logger) SwaggerController {
    controller := SwaggerController{
        logger:  logger,
    }
    return controller
}

func (this *SwaggerController) Setup(engine *gin.Engine) {
    docs.SwaggerInfo.Title = "Car Pooling Service"
    docs.SwaggerInfo.Description = "This service implements a Car Pooling service."
    docs.SwaggerInfo.Version = "1.0"
    docs.SwaggerInfo.BasePath = "/"

    engine.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))
}
