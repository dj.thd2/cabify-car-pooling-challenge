package controllers

import (
    "net/http"

    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/domains"
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/models"
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/lib"
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/api/middlewares"
    "github.com/gin-gonic/gin"
)

type CarPoolController struct {
    carPoolService               domains.CarPoolService
    contentTypeFilterMiddleware  middlewares.ContentTypeFilterMiddleware
    logger                       lib.Logger
}

func NewCarPoolController(carPoolService domains.CarPoolService, contentTypeFilterMiddleware middlewares.ContentTypeFilterMiddleware, logger lib.Logger) CarPoolController {
    controller := CarPoolController{
        carPoolService: carPoolService,
        contentTypeFilterMiddleware: contentTypeFilterMiddleware,
        logger:  logger,
    }
    return controller
}

func (this *CarPoolController) Setup(engine *gin.Engine) {
    engine.GET("/status", this.getStatus)
    engine.PUT("/cars", this.contentTypeFilterMiddleware.Handler("application/json"), this.putCars)
    engine.POST("/journey", this.contentTypeFilterMiddleware.Handler("application/json"), this.postJourney)
    engine.POST("/dropoff", this.contentTypeFilterMiddleware.Handler("application/x-www-form-urlencoded"), this.postDropoff)
    engine.POST("/locate", this.contentTypeFilterMiddleware.Handler("application/x-www-form-urlencoded"), this.postLocate)
    engine.NoRoute(this.invalidPath)
}

func (this *CarPoolController) invalidPath(ctx *gin.Context) {
    ctx.AbortWithStatus(http.StatusBadRequest)
}

// GetStatus godoc
// @Summary Health check (get status)
// @Schemes
// @Description Return a simple OK response
// @Tags health
// @Accept json
// @Produce json
// @Success 200 "Service is up and running"
// @Router /status [get]
func (this *CarPoolController) getStatus(ctx *gin.Context) {
    ctx.JSON(http.StatusOK, gin.H{
        "status": "ok",
    })
}

// PutCars godoc
// @Summary Load a list of all the available cars into the service
// @Schemes
// @Description Load a full list of the available cars that will be handled by the service, cleaning up all the previous existing data
// @Tags cars
// @Accept json
// @Param message body []models.Car true "Cars info"
// @Success 200 "The given car data has set successfully"
// @Failure 400 "Invalid data in the request or any car identifier is duplicated"
// @Router /cars [put]
func (this *CarPoolController) putCars(ctx *gin.Context) {
    var cars []*models.Car

    if err := ctx.BindJSON(&cars); err != nil {
        ctx.AbortWithStatus(http.StatusBadRequest)
        return
    }

    if err := this.carPoolService.ResetCars(cars); err != nil {
        responseStatus := http.StatusInternalServerError
        switch err {
            case domains.ErrDuplicatedID:
                responseStatus = http.StatusBadRequest
        }
        ctx.AbortWithStatus(responseStatus)
        return
    }

    ctx.Status(http.StatusOK)
}

// PostJourney godoc
// @Summary A group of people requests to perform a journey
// @Schemes
// @Description Given the group information (id and people count), a car will be assigned to them if any is available. If no cars are available at the moment, the group will be put in a queue
//              to be handled as soon as possible by its coming order.
// @Tags journeys
// @Accept json
// @Param message body models.Group true "Group info"
// @Success 202 "The journey was created successfully"
// @Failure 400 "Invalid data in the request or the group identifier is duplicated"
// @Router /journey [post]
func (this *CarPoolController) postJourney(ctx *gin.Context) {
    var group models.Group

    if err := ctx.BindJSON(&group); err != nil {
        ctx.AbortWithStatus(http.StatusBadRequest)
        return
    }

    if err := this.carPoolService.NewJourney(&group); err != nil {
        responseStatus := http.StatusInternalServerError
        switch err {
            case domains.ErrDuplicatedID:
                responseStatus = http.StatusBadRequest
        }
        ctx.AbortWithStatus(responseStatus)
        return
    }

    ctx.Status(http.StatusAccepted)
}

// PostDropoff godoc
// @Summary A group of people request to end a journey
// @Schemes
// @Description Given the group ID, their journey will end whether they travelled or not. If they were still waiting on the queue, they will be removed and nothing else will happen. If the group was travelling on an assigned car, the car will be freed and will be tried to be reassigned to another group or groups by their order in the queue until all the seats are filled.
// @Tags journeys, cars
// @Accept application/x-www-form-urlencoded
// @Param ID formData string true "Group ID"
// @Success 200 "The group was travelling on a car"
// @Success 204 "The group was waiting and never got served"
// @Failure 400 "Invalid data in the request"
// @Failure 404 "The group does not exist"
// @Router /dropoff [post]
func (this *CarPoolController) postDropoff(ctx *gin.Context) {
    var dropoff struct {
        ID uint `form:"ID" binding:"required"`
    }
    if err := ctx.Bind(&dropoff); err != nil {
        ctx.AbortWithStatus(http.StatusBadRequest)
        return
    }

    car, err := this.carPoolService.Dropoff(dropoff.ID)
    if err == domains.ErrNotFound {
        ctx.AbortWithStatus(http.StatusNotFound)
        return
    }
    if err != nil {
        ctx.JSON(http.StatusInternalServerError, gin.H{
            "error": err,
        })
        return
    }
    if car == nil {
        ctx.Status(http.StatusNoContent)
        return
    }

    ctx.Status(http.StatusOK)
}

// PostLocate godoc
// @Summary Locate assigned car to given group
// @Schemes
// @Description Given the group ID, get the car in which they are travelling or nothing in case they are still waiting to be served.
// @Tags journeys, cars
// @Accept application/x-www-form-urlencoded
// @Param ID formData string true "Group ID"
// @Success 200 {object} models.Car "The group is travelling on the returned car"
// @Success 204 "The group is waiting to be served"
// @Failure 400 "Invalid data in the request"
// @Failure 404 "The group does not exist"
// @Router /locate [post]
func (this *CarPoolController) postLocate(ctx *gin.Context) {
    var locate struct {
        ID uint `form:"ID" binding:"required"`
    }
    if err := ctx.Bind(&locate); err != nil {
        ctx.AbortWithStatus(http.StatusBadRequest)
        return
    }

    car, err := this.carPoolService.Locate(locate.ID)

    if err == domains.ErrNotFound {
        ctx.AbortWithStatus(http.StatusNotFound)
        return
    }
    if err != nil {
        ctx.JSON(http.StatusInternalServerError, gin.H{
            "error": err,
        })
        return
    }
    if car == nil {
        ctx.Status(http.StatusNoContent)
        return
    }

    ctx.JSON(http.StatusOK, car)
}
