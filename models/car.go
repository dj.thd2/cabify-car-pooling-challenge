package models

type Car struct {
    ID             uint `json:"id"`
    MaxSeats       uint `json:"seats"`
    AvailableSeats uint `json:"availableSeats"`
}
