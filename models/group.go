package models

type Group struct {
    ID         uint `json:"id"`
    People     uint `json:"people"`
    AssignedTo *Car `json:"-"`
}
