package domains

import (
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/models"
)

type CarPoolService interface {
    ResetCars(cars []*models.Car) error
    NewJourney(group *models.Group) error
    Dropoff(groupId uint) (car *models.Car, err error)
    Locate(groupId uint) (car *models.Car, err error)
}
