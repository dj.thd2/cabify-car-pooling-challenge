package domains

import (
    "errors"
)

const MinSeats = 4
const MaxSeats = 6

var ErrNotFound = errors.New("not found")
var ErrDuplicatedID = errors.New("duplicated ID")
var ErrInvalidState = errors.New("invalid state")
