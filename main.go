package main

import (
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/bootstrap"
    "github.com/joho/godotenv"
)

// Application main entry point
func main() {
    _ = godotenv.Load()
    bootstrap.RootApp.Execute()
}
