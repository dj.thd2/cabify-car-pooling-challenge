package services

import (
    "sync"
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/domains"
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/lib"
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/models"
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/repository"
)

type CarPoolService struct {
    logger     lib.Logger
    groupRepository repository.GroupRepository
    carRepository repository.CarRepository
    mutex *sync.RWMutex
}

func NewCarPoolService(logger lib.Logger, groupRepository repository.GroupRepository, carRepository repository.CarRepository) domains.CarPoolService {
    return CarPoolService{
        logger:     logger,
        groupRepository: groupRepository,
        carRepository: carRepository,
        mutex: &sync.RWMutex{},
    }
}

func (this CarPoolService) dropoff(car *models.Car, passengerCount uint) error {
    if (car.AvailableSeats + passengerCount) > car.MaxSeats {
        this.logger.Fatal("Invalid state in dropoff, the available seats of the car will be over its maximum seats")
        return domains.ErrInvalidState
    }

    car.AvailableSeats += passengerCount
    return nil
}

func (this CarPoolService) pickup(car *models.Car, passengerCount uint) error {
    if passengerCount > car.AvailableSeats {
        this.logger.Fatal("Invalid state in pickup, trying to put more people on the car than its available seats")
        return domains.ErrInvalidState
    }

    car.AvailableSeats -= passengerCount
    return nil
}

func (this CarPoolService) reassign(car *models.Car) (err error) {
    for {

        group, err := this.groupRepository.GetNextGroupByMaxPeople(car.AvailableSeats)
        if err != nil {
            break
        }

        if group == nil {
            break
        }

        if car.AvailableSeats < group.People {
            this.logger.Fatal("A car without enough available seats was returned, this should never happen")
            break
        }

        group.AssignedTo = car
        this.pickup(car, group.People)

        this.logger.Infof(">> Car %d reassigned to group %d", car.ID, group.ID)

        if err := this.groupRepository.Save(group); err != nil {
            this.logger.Fatal("error updating group, this should never happen")
            break
        }

        if car.AvailableSeats == 0 {
            break
        }
    }

    return err
}

func (this CarPoolService) ResetCars(cars []*models.Car) error {
    this.mutex.Lock()
    defer this.mutex.Unlock()

    this.groupRepository.DeleteAll()

    this.carRepository.DeleteAll()
    for _, car := range cars {
        car.AvailableSeats = car.MaxSeats
        if err := this.carRepository.Create(car); err != nil {
            this.carRepository.DeleteAll()
            return err
        }
    }

    return nil
}

func (this CarPoolService) NewJourney(group *models.Group) error {
    var selectedCar *models.Car

    for i := group.People; i <= domains.MaxSeats; i++ {
        car := this.carRepository.FindNextWithAvailableSeats(i)
        if car != nil {
            selectedCar = car
            break
        }
    }

    if selectedCar != nil {
        group.AssignedTo = selectedCar
    }

    if err := this.groupRepository.Create(group); err != nil {
        if selectedCar != nil {
            this.carRepository.EnqueueIntoAvailableList(selectedCar)
        }
        return err
    }

    if selectedCar == nil {
        this.groupRepository.EnqueueIntoPendingList(group)
    } else {
        this.pickup(selectedCar, group.People)
        this.carRepository.EnqueueIntoAvailableList(selectedCar)
    }

    return nil
}

func (this CarPoolService) Dropoff(groupId uint) (*models.Car, error) {
    group, err := this.groupRepository.Find(groupId)
    if err != nil {
        return nil, err
    }

    if group == nil {
        return nil, domains.ErrNotFound
    }

    car := group.AssignedTo

    if err := this.groupRepository.Delete(groupId); err != nil {
        return nil, err
    }

    if car == nil {
        return nil, nil
    }

    err = this.carRepository.UnqueueFromAvailableList(car)
    if err != nil {
        return nil, err
    }

    err = this.dropoff(car, group.People)

    if err == nil {
        err = this.reassign(car)
    }

    this.carRepository.EnqueueIntoAvailableList(car)

    return car, err
}

func (this CarPoolService) Locate(groupId uint) (car *models.Car, err error) {
    group, err := this.groupRepository.Find(groupId)

    if err != nil {
        return nil, err
    }

    if group == nil {
        return nil, domains.ErrNotFound
    }

    return group.AssignedTo, nil
}
