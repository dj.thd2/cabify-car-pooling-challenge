package bootstrap

import (
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/api/controllers"
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/api/middlewares"
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/api/routes"
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/lib"
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/repository"
    "gitlab-hiring.cabify.tech/cabify/interviewing/car-pooling-challenge-go/services"
    "go.uber.org/fx"
)

var CommonModules = fx.Options(
    controllers.Module,
    middlewares.Module,
    routes.Module,
    lib.Module,
    services.Module,
    repository.Module,
)
